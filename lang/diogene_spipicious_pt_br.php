<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/diogene_spipicious?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'explication_tags' => 'Esta opção usará o plugin SPIPicious para propor aos autores que incluam tags nas suas publicações',

	// F
	'form_legend' => 'Tags',

	// L
	'label_remove_tags' => 'Supressão das Tags',
	'label_tags' => 'Inclua uma ou mais tag(s)',

	// T
	'tags_spipicious' => 'Tags'
);
