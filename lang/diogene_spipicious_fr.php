<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/diogene_spipicious.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'explication_tags' => 'Cette option utilisera le plugin SPIPicious pour proposer aux auteurs de rajouter des tags sur leurs mises en ligne',

	// F
	'form_legend' => 'Tags',

	// L
	'label_remove_tags' => 'Suppression des Tags',
	'label_tags' => 'Ajoutez un ou plusieurs tag(s)',

	// T
	'tags_spipicious' => 'Tags'
);
