<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/diogene_spipicious.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'diogene_spipicious_description' => 'Complément "tags personnels" pour "Diogene" (utilise SpipIcious)',
	'diogene_spipicious_slogan' => 'Complément "tags personnels"'
);
